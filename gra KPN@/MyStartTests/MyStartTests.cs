﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using graKPN;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Linq;

namespace MyStartTests

{
    [TestClass]
    public class MyStartTests
    {
        private static IPAddress LocalIPAddress()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }

        [TestMethod]
        [ExpectedException(typeof(SocketException),  "IP adress jest nie poprawny.")]

        public void ClientIpAdressExceptionTest()
        {
            StartC startC = new StartC();
            TextBox ctrl = new TextBox();
            ctrl.Text = "999.999.999.999";
            startC.StartClient(ctrl);
        }

        [TestMethod]
        public void LocalIPisNotNulNorEmptyTest()
        {
            try
            {
                Assert.AreNotEqual(LocalIPAddress().ToString(), string.Empty);
                Assert.IsNotNull(LocalIPAddress().ToString());
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }
    }
}
