﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace graKPN
{
    public partial class Form1 : Form
        
    {
        StreamReader reader;
        StreamWriter writer;
        StartC startC;
        StartS startS;
        bool send_hand = false;
        bool read_hand = false;
        string my_hand = "";
        string op_hand = "" ;

        public bool SetButtons1 { get; set; }
        /// <summary>
        /// aby uzyskać adres IPv4 interfejsu sieci Ethernet, 
        /// </summary>
        /// <returns></returns>
        private IPAddress LocalIPAddress()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }
       
        public Form1()
        {           
            InitializeComponent();
            SetButtons(false);
            try
            {
                text_ip.Text = LocalIPAddress().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// tworzymy  set przyciskow 
        /// </summary>
        /// <param name="enable"></param>
        private void SetButtons(bool enable)
        {
            button1.Enabled = enable;
            button2.Enabled = enable;
            button3.Enabled = enable;
        }
        /// <summary>
        /// włączenie przyciskow po podłączeniu serwera i clienta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_start_Click(object sender, EventArgs e)
        {
            if (radio_server.Checked)
                StartServer();
            if (radio_client.Checked)  
                StartClient();
            SetButtons(true);
            timer.Enabled = true;
        }

        /// <summary>
        /// podłączenia funkcjonala z klasy startS
        /// </summary>
        public void StartServer()
        {
            startS = new StartS();
            startS.StartServer(text_ip);
            reader = new StreamReader(startS.getServer().GetStream());
            writer = new StreamWriter(startS.getServer().GetStream());
            writer.AutoFlush = true;
        }
        /// <summary>
        /// podłączenia funkcjonala z klasy startC
        /// </summary>
        public void StartClient()
        {
            startC = new StartC();
            startC.StartClient(text_ip);
            reader = new StreamReader(startC.getServer().GetStream());
            writer = new StreamWriter(startC.getServer().GetStream());
            writer.AutoFlush = true;
        }
        /// <summary>
        /// tworzymy object przycisk i implementujemy waysylac  zancenia  pzy nacisku " K"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {
            Send("K");
        }
        /// <summary>
        /// tworzymy object przycisk i implementujemy waysylac  zancenia  pzy nacisku " P"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, EventArgs e)
        {
            Send("P");
        }
        /// <summary>
        /// tworzymy object przycisk i implementujemy waysylac  zancenia  pzy nacisku " N"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button3_Click(object sender, EventArgs e)
        {
            Send("N");
        }

        /// <summary>
        /// wysyłanie danych z przycisków i wyłączenie ostatnich 
        /// </summary>
        /// <param name="text"></param>
        private void Send (string text )
        {
            if (send_hand) return;
                         writer.WriteLine(text);
           send_hand = true;
              my_hand = text;
                SetButtons(false);
            
                
        }

        /// <summary>
        /// otrzymane dane pokazuje co jest otrzymane jezeli nic to wynik bede pusty
        /// </summary>
        /// <returns> wyswitla text </returns>
        private string read ()
        {
            if (read_hand) return ""; 
            try
            {
                string text;
                text = reader.ReadLine();
                read_hand = true;
                op_hand = text;
                 return text;

            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// wykorzystana dla szybkiego i widocznego debugu dzialania innych metod
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e">argumet </param>
        private void timer_Tick(object sender, EventArgs e)
        {

            text_debug.Text =
                "send_hand = " + send_hand.ToString() + Environment.NewLine +
                "read_hand = " + read_hand.ToString() + Environment.NewLine +
                  "my_hand = " + my_hand + Environment.NewLine +
                  "op_hand = " + op_hand;

            string hand = read();
            if (send_hand && read_hand)
                FinishGame(); 
           }
        /// <summary>
        ///  logika gry ,jedziemy na ifach i dajemy znaczenia reka opowiadanie z literamy
        /// </summary>
        /// <param name="hand1">ręka moja</param>
        /// <param name="hand2">ręka oponenta</param>
        /// <returns></returns>
        private int CompareHands(string hand1, string hand2)
        {
            if (hand1 == hand2) return 0;
            if (hand1 == "K")
                if (hand2 == "N")
                    return 1;
                else
                    return 2;
            ///
            if (hand1 == "N")
                if (hand2 == "P")
                    return 1;
                else
                    return 2;
            ///
            if (hand1 == "P")
                if (hand2 == "K")
                    return 1;
                else
                    return 2;
            return 0;
        }
        /// <summary>
        /// odczyt danych z metody Comprare Hands wyswietlanie resultata 
        /// </summary>
        private void FinishGame()
        {
            int ch = CompareHands(my_hand, op_hand);
            string res = "";
            if (ch == 0) res = "remis";
            if (ch == 1) res = "zwycięstwo";
            if (ch == 2) res = "strata";
            resultat.Text = res;
            send_hand = false;
            read_hand = false;
            SetButtons1 = true;
            my_hand = "";
            op_hand = "";
        }

        
       
    }
}
