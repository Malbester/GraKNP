﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;

namespace graKPN
{
    /// <summary>
    /// 
    /// </summary>
    public class StartC :  Start
    {
        int port = 1;
        StreamReader reader;
        StreamWriter writer;
        /// <summary>
        /// Odpowiada za tworzenie  clienta  podłączenie i odczytywanie danych z pewnym interwalem z podanego ip 
        /// </summary>
        /// <param name="ctrl">otczyt danych z kontrolki</param>
        public void StartClient(TextBox ctrl)
        {
            tcpClient = new TcpClient();
            tcpClient.Connect(ctrl.Text, port);
            tcpClient.ReceiveTimeout = 50;
            reader = new StreamReader(tcpClient.GetStream());
            writer = new StreamWriter(tcpClient.GetStream());
            writer.AutoFlush = true;
        }
    }
}
