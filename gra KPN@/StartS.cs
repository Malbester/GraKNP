﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;

namespace graKPN
{
    public class StartS : Start
    {
        int port = 1;
        /// <summary>
        /// odpowiada za tworzenie  serwera i sposob podłączenie i odczytywanie danych z pewnym interwalem 
        /// </summary>
        /// <param name="ctrl">otczyt danych z kontrolki </param>
        public void StartServer(TextBox ctrl)
        {
            IPEndPoint endp = new IPEndPoint(IPAddress.Parse(ctrl.Text), port);
            TcpListener listener = new TcpListener(endp);       
            listener.Start();                                                                            
            tcpClient = listener.AcceptTcpClient();                                                
            tcpClient.ReceiveTimeout = 50;
        }
    }
}
