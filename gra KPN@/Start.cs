﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace graKPN
{
    public abstract class Start
    {
        
        protected TcpClient tcpClient;
        /// <summary>
        /// wykorzystane przez dwa klassa StartS,StartC 
        /// </summary>
        /// <returns>Transfer danych za pomocą socket</returns>
        public TcpClient getServer()
        {
            return tcpClient;
        }
    }
}
