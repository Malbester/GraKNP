﻿using System;

namespace graKPN
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.radio_server = new System.Windows.Forms.RadioButton();
            this.radio_client = new System.Windows.Forms.RadioButton();
            this.text_ip = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button_start = new System.Windows.Forms.Button();
            this.resultat = new System.Windows.Forms.Label();
            this.text_debug = new System.Windows.Forms.TextBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // radio_server
            // 
            this.radio_server.AutoSize = true;
            this.radio_server.Location = new System.Drawing.Point(21, 30);
            this.radio_server.Name = "radio_server";
            this.radio_server.Size = new System.Drawing.Size(56, 17);
            this.radio_server.TabIndex = 0;
            this.radio_server.TabStop = true;
            this.radio_server.Text = "Server";
            this.radio_server.UseVisualStyleBackColor = true;
            // 
            // radio_client
            // 
            this.radio_client.AutoSize = true;
            this.radio_client.Location = new System.Drawing.Point(21, 53);
            this.radio_client.Name = "radio_client";
            this.radio_client.Size = new System.Drawing.Size(54, 17);
            this.radio_client.TabIndex = 1;
            this.radio_client.TabStop = true;
            this.radio_client.Text = "Client ";
            this.radio_client.UseVisualStyleBackColor = true;
            // 
            // text_ip
            // 
            this.text_ip.Location = new System.Drawing.Point(21, 78);
            this.text_ip.Name = "text_ip";
            this.text_ip.Size = new System.Drawing.Size(114, 20);
            this.text_ip.TabIndex = 2;
            this.text_ip.Text = "192.168.0.16";
            this.text_ip.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.text_ip.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(195, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 31);
            this.button1.TabIndex = 3;
            this.button1.Text = "Kamien";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(195, 67);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(141, 31);
            this.button2.TabIndex = 4;
            this.button2.Text = "Papier";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(195, 104);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(141, 31);
            this.button3.TabIndex = 5;
            this.button3.Text = "Nozyczki";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(21, 112);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(114, 23);
            this.button_start.TabIndex = 6;
            this.button_start.Text = "START !";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.Button_start_Click);
            // 
            // resultat
            // 
            this.resultat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.resultat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultat.Location = new System.Drawing.Point(195, 138);
            this.resultat.Name = "resultat";
            this.resultat.Size = new System.Drawing.Size(141, 27);
            this.resultat.TabIndex = 7;
            this.resultat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // text_debug
            // 
            this.text_debug.Location = new System.Drawing.Point(21, 196);
            this.text_debug.Multiline = true;
            this.text_debug.Name = "text_debug";
            this.text_debug.Size = new System.Drawing.Size(114, 116);
            this.text_debug.TabIndex = 8;
            this.text_debug.Text = " ";
            this.text_debug.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(348, 184);
            this.Controls.Add(this.text_debug);
            this.Controls.Add(this.resultat);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.text_ip);
            this.Controls.Add(this.radio_client);
            this.Controls.Add(this.radio_server);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "  graKPN";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

       

        #endregion

        private System.Windows.Forms.RadioButton radio_server;
        private System.Windows.Forms.RadioButton radio_client;
        private System.Windows.Forms.TextBox text_ip;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Label resultat;
        private System.Windows.Forms.TextBox text_debug;
        private System.Windows.Forms.Timer timer;
    }
}

