﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace graKPN
{
    public partial class Form1 : Form
    {
        int port = 1;
        StreamReader reader;
        StreamWriter writer;
        bool send_hand = false;
        bool read_hand = false;
        string my_hand = "";
        string op_hand = "" ;
        private bool setButtons;

        public bool SetButtons1 { get => setButtons; set => setButtons = value; }

        public Form1()
        {
            InitializeComponent();
            SetButtons(false);

        }

        private void SetButtons(bool enable)
        {
            button1.Enabled = enable;
            button2.Enabled = enable;
            button3.Enabled = enable;
        }
        private void Button_start_Click(object sender, EventArgs e)
        {
            if (radio_server.Checked)
                StartServer();
            if (radio_client.Checked)  
                StartClient();
            SetButtons(true);
            timer.Enabled = true;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void StartServer()
        {
            TcpListener listener = new TcpListener(new IPEndPoint(IPAddress.Parse(text_ip.Text), port));///sozdajem sluhatelia kotoryj sluszaje po ukazanomu ip  i poru 
            listener.Start();///zapuskajem  jego 
            TcpClient server = listener.AcceptTcpClient();/// otmiecajem na dzwon - eta operacuja zdet podtmierdzenia 
            server.ReceiveTimeout = 50;
            reader = new StreamReader(server.GetStream());
            writer = new StreamWriter(server.GetStream());
            writer.AutoFlush = true;
                     
        }

        private void StartClient()
        {
            TcpClient client = new TcpClient();
            client.Connect(text_ip.Text, port);///,,,,<<<<
            client.ReceiveTimeout = 50;
            reader = new StreamReader(client.GetStream());
            writer = new StreamWriter(client.GetStream());
            writer.AutoFlush = true; 
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Send("K");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Send("P");
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Send("N");
        }


        private void Send (string text )
        {
            if (send_hand) return;
                         writer.WriteLine(text);
           send_hand = true;
              my_hand = text;
                SetButtons(false);
            
                
        }
        

        private string read ()
        {
            if (read_hand) return ""; 
            try
            {
                string text;
                text = reader.ReadLine();
                read_hand = true;
                op_hand = text;
                 return text;

            }
            catch
            {
                return "";
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {

            text_debug.Text =
                "send_hand = " + send_hand.ToString() + Environment.NewLine +
                "read_hand = " + read_hand.ToString() + Environment.NewLine +
                  "my_hand = " + my_hand + Environment.NewLine +
                  "op_hand = " + op_hand;

            string hand = read();
            if (send_hand && read_hand)
                FinishGame();
           }

        private int CompareHands(string hand1, string hand2)
        {
            if (hand1 == hand2) return 0;
            if (hand1 == "K")
                if (hand2 == "N")
                    return 1;
                else
                    return 2;
            ///
            if (hand1 == "N")
                if (hand2 == "P")
                    return 1;
                else
                    return 2;
            ///
            if (hand1 == "P")
                if (hand2 == "K")
                    return 1;
                else
                    return 2;
            return 0;
        }

        private void FinishGame()
        {
            int ch = CompareHands(my_hand, op_hand);
            string res = "";
            if (ch == 0) res = "remis";
            if (ch == 1) res = "zwycięstwo";
            if (ch == 2) res = "strata";
            resultat.Text = res;
            send_hand = false;
            read_hand = false;
            SetButtons1 = true;
            my_hand = "";
            op_hand = "";
        }

        
       
    }
}
